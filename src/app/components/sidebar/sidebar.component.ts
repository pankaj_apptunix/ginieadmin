import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent implements OnInit {
  user: any;
  menuItems = [];
  dashboard: any;
  category: string;
  product: string;
  subcategory: string;
  order: string;
  setting: string;
  subadmin: string;
  driver: string;
  provider: string;
  coupon: string;
  service: string;
  offer: string;
  role: any;
  access: any;
  booking: any;
  attribute: any;
  giftcard: any;
  contact: any;
  suggestion: string;
  cms: any;

  constructor() {
    this.dashboard = 'assets/icons/Dashboard.svg',
     this.user = 'assets/icons/User.svg',
    this.category = 'assets/icons/Category.svg'
    this.product = 'assets/icons/product.png'
    // this.subcategory = 'assets/icons/sub-category.png'
    // this.order = 'assets/icons/order.png'
    // this.setting = 'assets/icons/gear.png'
    this.subadmin = 'assets/icons/sub-admin.png'
    this.driver = 'assets/icons/driver-icon.png'
    this.provider = 'assets/icons/Provider Management.svg'
    this.coupon = 'assets/icons/Commision.svg'
    this.service = 'assets/icons/Service Management.svg'
    this.offer = 'assets/icons/offer.png',
    this.giftcard = 'assets/icons/gift-card.png',
    this.contact = 'assets/icons/Contact.svg'
    this.booking = 'assets/icons/Booking.svg'
    this.suggestion ='assets/icons/Suggestion.svg',
    this.cms ='assets/icons/CMS.svg'

  }
  // user:any;


  ngOnInit() {
    this.menuItems = [
      { path: '/dashboard/home', title: 'Dashboard', icon: this.dashboard, class: '' },
      { path: '/dashboard/users', title: 'User Management', icon: this.user, class: '' },
      { path: '/dashboard/provider', title: 'Provider Management', icon: this.provider, class: '' },
      { path: '/dashboard/categories', title: 'Category', icon: this.category, class: '' },
      { path: '/dashboard/service', title: 'Service Management', icon: this.service, class: '' },
      {path: '/dashboard/bookings', title: 'Bookings', icon: this.booking, class: ''},
      {path: '/dashboard/contact', title: 'Contact', icon: this.contact, class: ''},
      {path: '/dashboard/suggestions', title: 'Suggestions', icon: this.suggestion, class: ''},
      {path: '/dashboard/cms', title: 'CMS Pages', icon: this.cms, class: ''},
      {path:'/dashboard/commission', title: 'Commission', icon: this.coupon, class: ''}

      // 


    ];


  }

}
