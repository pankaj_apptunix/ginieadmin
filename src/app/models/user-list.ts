export class UserList {
  points: number;
  isVerified: boolean;
  isApproved: boolean;
  isBlocked: boolean;
  _id: string;
  userName: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  address: string;
  country: string;
  state: string;
  city: string;
  role: string;
  totalAmount:any;
  paybleAmount :any
  amountPaid:any
  createdAt: string;
  updatedAt: string;
  id: string;
}
